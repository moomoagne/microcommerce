package com.ecommerce.microcommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class MicrocommerceApplication {

    //http://localhost:9090/v2/api-docs
    //http://localhost:9090/swagger-ui.html
	public static void main(String[] args) {
		SpringApplication.run(MicrocommerceApplication.class, args);
	}
}
