package com.ecommerce.microcommerce.web.controller.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ProduitIntrouvableExceptions extends RuntimeException {
    public ProduitIntrouvableExceptions(String s) {
        super(s);
    }
}
