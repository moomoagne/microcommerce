package com.ecommerce.microcommerce.web.controller;

import com.ecommerce.microcommerce.dao.ProductDao;
import com.ecommerce.microcommerce.model.Product;
import com.ecommerce.microcommerce.web.controller.exceptions.ProduitIntrouvableExceptions;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@Api(description = "API pour les operation de CRUD sur les produits")
@RestController
public class ProductController {

    @Autowired
    private ProductDao productDao;

    @ApiOperation(value = "Récupére la liste de tout les produits")
    @RequestMapping(value = "/Produits", method=RequestMethod.GET)
    public MappingJacksonValue productList(){
        List<Product> produits = productDao.findAll();
        SimpleBeanPropertyFilter monFiltre = SimpleBeanPropertyFilter.serializeAllExcept("prixAchat");
        FilterProvider listDeNosFiltres = new SimpleFilterProvider().addFilter("monFiltreDynamique", monFiltre);
        MappingJacksonValue produitsFiltres = new MappingJacksonValue(produits);
        produitsFiltres.setFilters(listDeNosFiltres);
        return  produitsFiltres;
    }

    @ApiOperation(value = "Récupére un produit grace a son ID")
    @GetMapping(value = "/Produits/{id}")
    public Product afficherProduit(@PathVariable int id){
        Product produit =productDao.findById(id);
        if (produit == null){
            throw new ProduitIntrouvableExceptions("Le produit avec l'id "+id+ " est introuvable");
        }
        return produit;
    }


    @GetMapping(value = "test/produits/{prixLimit}")
    public List<Product> testGetProduitAvecLimit(@PathVariable int prixLimit){
        //Lites des produits plus grande que 400
        return productDao.findByPrixGreaterThan(prixLimit);
    }

    @GetMapping(value = "test/produits/{recherche}")
    public List<Product> testGetProduitAvecLimit(@PathVariable String recherche){
        //Rechet avec des mot cle recherche
        return productDao.findByNomLike("%"+recherche+"%");
    }

    @ApiOperation(value = "Ajouter un produit")
    @PostMapping(value = "/Produits")
    public ResponseEntity<Void> ajouterProduit(@Valid @RequestBody Product product) {
        Product productAdded = productDao.save(product);

        if (productAdded == null)
            return ResponseEntity.noContent().build();

            URI location = ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(productAdded.getId())
                    .toUri();

            return ResponseEntity.created(location).build();

    }

    @ApiOperation(value = "Supprimer un produit")
    @DeleteMapping(value = "/Produits/{id}")
    public void deleteProduit(@PathVariable int id){
        productDao.delete(id);
    }

    @ApiOperation(value = "Modifier un produit")
    @PutMapping(value = "/Produits")
    public void updateProduit(@PathVariable Product product){
        productDao.save(product);
    }

    //Rechercher un produit cher
    @GetMapping(value = "test/produits/Cher/{prixLimit}")
    public List<Product> rechercherProduitCher(@PathVariable int prixLimit){
        //Lites des produits chers
        return productDao.checherUnProduitCher(prixLimit);
    }

}
