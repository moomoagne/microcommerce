package com.ecommerce.microcommerce.dao;

import com.ecommerce.microcommerce.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductDao extends JpaRepository<Product, Integer> {
   // public List<Product> findAll();
     Product findById(int id);
     List<Product> findByPrixGreaterThan(int prixLimit);
     List<Product> findByNomLike(String recherche);

     //Les @Query ou JPQL
    @Query("SELECT id, nom, prix FROM Product p WHERE p.prix > :prixLimit")
    List<Product> checherUnProduitCher(@Param("prixLimit") int prix);

}
